import { Module } from "@nestjs/common";
import { ContextModule } from "./contexts/context.module";
import { ProvidersModule } from "./providers/providers.module";

@Module({
    imports: [ProvidersModule, ContextModule],
    exports: [ProvidersModule, ContextModule]
})

export class InfraModule { }