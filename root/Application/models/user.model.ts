import { ApiProperty } from "@nestjs/swagger";
import { AccountModel } from "./account.model";
import { ProfileModel } from "./profile.model";
import { IsNotEmpty } from 'class-validator';

export class UserModel {
    id: string;

    @IsNotEmpty()
    @ApiProperty({ type: 'string' })
    name: string;

    @IsNotEmpty()
    @ApiProperty({ type: 'string' })
    surname: string;

    @IsNotEmpty()
    @ApiProperty({ type: 'string' })
    account: AccountModel;
}