import { Inject, Injectable } from "@nestjs/common";
import { User } from "root/Domain/entities/user.entity";
import { IUserRepository } from "root/Domain/interfaces/repositories/IUser.repository";
import { UserRepository } from "root/Infra/contexts/user/user.repository";
import { UserModel } from "../../../models/user.model";
import { IUserService } from "../../interfaces/IUser.interface";

@Injectable()
export class UserService implements IUserService {
    /* 
        @@TODO 
        Mapper Model to/from Domain 
    */

    constructor(
        @Inject(UserRepository) private readonly userRepository: IUserRepository) {
        // console.log('consumerService', this.consumerService);
    }

    public async get(id: string): Promise<UserModel> {
        const domainResult = await this.userRepository.getEntity({ id });
        const modelResult: UserModel = {
            ...domainResult
        }
        return modelResult;
    }

    public async list(): Promise<Array<UserModel>> {
        const domainResult: Array<User> = await this.userRepository.listEntity();
        const modelResult: Array<UserModel> = {
            ...domainResult
        }

        return modelResult;
    }

    public async create(model: UserModel) {
        const domainEntity: User = {
            ...model
        }; 

        const domainResult: User = await this.userRepository.saveEntity(domainEntity);

        const modelResult: UserModel = {
            ...domainResult
        }
        return modelResult
    }

    public async update(model: UserModel) {
        const domainEntity: User = {
            ...model
        };

        const domainResult: User = await this.userRepository.updateEntity(domainEntity);

        const modelResult: UserModel = {

            ...domainResult
        }
        return modelResult
    }

    public async delete(id: string) {
        return await this.userRepository.deleteEntity(id);
    }

}